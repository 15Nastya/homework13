import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom"

function ProductPage() {
    const { id } = useParams();
    const [  product, setProduct] = useState({});

    useEffect(() => {
        fetch (`https://fakestoreapi.com/products/${id}`)
          .then((response) => response.json())
          .then((result) => {
            setProduct(result);
          });
    })

    return (
        <>
            <h1>{ product.title}</h1>
            <img src={product.image}></img>
            <div>
                { product.description }
            </div>
        </>
    )
}

export default ProductPage;

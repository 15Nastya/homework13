import { Link } from 'react-router-dom';
import './ListItem.scss'; 

function ListItem ({ img, title, price, id }){
    return (
        <Link to={`/products/${id}`}className= {'ListItem'}>
            <div className= {'ListItem__img'}>
                <img src={img}></img>
            </div>
            <h2 className= {'ListItem__title'}>{title}</h2>
            <div className= {'ListItem__price'}>{price}</div>
            <button className= {'ListItem__button'}>Добавить в корзину</button>
        </Link>
    )
}

export default ListItem;  